﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTile : MonoBehaviour
{
    public enum FloorType
    { Normal, Ice, Start, Goal, Null, FastFall }

    public FloorType m_FloorType = FloorType.Normal;

    public float Health = 1000f;

    public float FallSpeed = 5f;

    public int Xpos = 0;
    public int Ypos = 0;

    private float countdown = -10;

    public bool m_isIce = false;

    bool death = false;

    private void Update()
    {
        if (m_FloorType == FloorType.Normal || m_FloorType == FloorType.Ice)
        {
            NormalTileState();
        }
        else
        if (m_FloorType == FloorType.FastFall)
        {
            FastFallState();
        }
        else
        if (m_FloorType == FloorType.Goal)
        {
            goalstate();
        }
    }

    private void FastFallState()
    {
        if (Health <= 0f)
        {
            if (death == false)
            {
                death = true;
            }

            DeathState();
            return;
        }

        Health -= Time.deltaTime * 1.5f;
    }

    private void NormalTileState()
    {
        if (Health <= 0f)
        {
            if (death == false)
            {
                death = true;
            }

            DeathState();
            return;
        }

        Health -= Time.deltaTime * 0.5f;
    }

    private void DeathState()
    {
        transform.position += Vector3.down * Time.deltaTime * FallSpeed;

        if (transform.position.y <= (FallSpeed * -2f ))
            Destroy(gameObject);
    }

    public void StageVictory(float time)
    {
        countdown = time;
    }

    private void WinState(float time)
    {
        countdown = time;

        if (countdown >= 0)
        {
            transform.position += Vector3.up * Time.deltaTime * FallSpeed;
        }
    }

    private void goalstate()
    {
        if(countdown > -1f)
        {

            Player p = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

            Vector3 thePos = p.transform.position - transform.position;
            thePos.y = 0f;

            if (thePos.magnitude >= 2f)
                return;

            WinState(countdown - Time.deltaTime);
        }
    }

    public void givePos(Vector2 pos)
    {
        Xpos = (int)pos.x;
        Ypos = (int)pos.y;
    }

    public void givePos(Vector3 pos)
    {
        Xpos = (int)pos.x;
        Ypos = (int)pos.z;
    }

    public bool PosCheck(Vector2 pos)
    {
        if ((Xpos == (int)pos.x) &&
        (Ypos == (int)pos.y))
            return true;
        else return false;
    }

    public bool PosCheck(Vector3 pos)
    {
        if ((Xpos == (int)pos.x) &&
        (Ypos == (int)pos.z))
            return true;
        else return false;
    }
}
