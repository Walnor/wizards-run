﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Telleport : MonoBehaviour, TriggerEffect
{
    public Telleport m_Other;

   // public GameObject soundOBJ;

    public GameObject WarpParticle;

    public void TrigEffect(Player p)
    {
        Vector3 Dest = m_Other.gameObject.transform.position;
        Dest.y += 1f;
        Destroy(m_Other.gameObject);

        Vector3 distanceDifference = Dest - transform.position;

        CameraController CC = FindObjectOfType<CameraController>();
        CC.gameObject.transform.position += distanceDifference;

        Transform tCam = FindObjectOfType<CameraController>().gameObject.transform;

        //GameObject objSound = Instantiate(soundOBJ, tCam);
        //objSound.transform.localPosition = Vector3.zero;

        GameObject ptcl = Instantiate(WarpParticle, p.transform);
        ptcl.transform.localPosition = new Vector3(0, -0.5f, 0);

        p.transform.position = Dest;
    }
}
