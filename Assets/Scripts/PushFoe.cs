﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushFoe : MonoBehaviour, Enemy
{
    Player target = null;

    float TargetDist = 1f;

    float m_speed = 4f;

    float m_Stun = 0f;

    float MaxRange = 15f;

    private void Update()
    {

        if (target == null)
        {
            target = findPlayer();
            if (target == null)
                return;
        }
        if (Time.timeScale == 0f || target.EndState)
            return;
        if (m_Stun <= 0f)
        {
            PushState();
        }
        else
        {
            StunState();
        }
    }

    private void PushState()
    {
        Vector3 distance = target.gameObject.transform.position - transform.position;
        distance.y = 0;

        if (distance.magnitude > MaxRange)
            return;

        if (distance.magnitude > TargetDist)
        {
            transform.position += distance.normalized * Time.deltaTime * m_speed;
        }

        transform.LookAt(target.transform);
    }

    private void StunState()
    {
        Vector3 distance = target.gameObject.transform.position - transform.position;
        distance.y = 0;
        transform.position -= distance.normalized * Time.deltaTime * m_speed * 2f;
        m_Stun -= Time.deltaTime;
    }

    private void StallState()
    {
    }

    private Player findPlayer()
    {
        return GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    public void OnHit(float Power)
    {
        m_Stun += Power * 0.5f;
    }

    public new dataFoe.foeType GetType()
    {
        return dataFoe.foeType.Push;
    }
}
