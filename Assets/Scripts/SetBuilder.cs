﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetBuilder : MonoBehaviour
{
    public Text SetName;

    public Text StageName;

    public GameObject TextPrefab;

    public GameObject btn_Left, btn_Right;

    public RectTransform Pannel;

    List<string> AvailableFiles = new List<string>();

    List<string> AddedFiles = new List<string>();

    Queue<GameObject> StageTextObjects = new Queue<GameObject>();

    int index = 0;
    private void Start()
    {
        DirectoryInfo d = new DirectoryInfo(@".\Assets\StageFiles");
        FileInfo[] Files = d.GetFiles("*.txt");

        foreach (FileInfo f in Files)
        {
            AvailableFiles.Add(f.Name);
        }
        EndEffect();
    }

    private void Update()
    {
        while (StageTextObjects.Count > 0)
        {
            GameObject obj = StageTextObjects.Dequeue();
            Destroy(obj);
        }
        for (int i = 0; i < AddedFiles.Count; i++)
        {
            GameObject obj = Instantiate(TextPrefab, Pannel);

            Vector3 newPos = Pannel.transform.position;
            newPos.y += Pannel.rect.height / 3f;
            newPos.y -= 50 * i;

            obj.transform.position = newPos;

            Vector3 newScale = new Vector3(4f, 1.25f, 1);

            obj.transform.localScale = newScale;

            obj.GetComponent<Text>().text = AddedFiles[i];

            StageTextObjects.Enqueue(obj);
        }
    }

    public void SaveSet()
    {
        string toSave = "";
        for (int i = 0; i < AddedFiles.Count; i++)
        {
            toSave += AddedFiles[i];
            if (i + 1 != AddedFiles.Count)
                toSave += "\n";
        }        

        List<string> rows = new List<string>();
        rows.AddRange(toSave.Split('\n'));


        string filePath = @".\Assets\StageSetFiles\" + SetName.text + ".txt";

        FileStream fs = File.Create(filePath);

        StreamWriter sw = new StreamWriter(fs);
        sw.Flush();

        foreach (string s in rows)
        {
            sw.WriteLine(s);
        }

        sw.Close();
        Debug.Log(toSave);
    }

    public void AddStage()
    {
        AddedFiles.Add(StageName.text);
        AvailableFiles.Remove(StageName.text);

        index--;

        if (index <= 0)
            index = 0;

        EndEffect();
    }


    public void Right()
    {
        index++;
        EndEffect();
    }

    public void Left()
    {
        index--;
        EndEffect();
    }

    private void EndEffect()
    {
        StageName.text = AvailableFiles[index];

        if (index == 0)
        {
            btn_Left.SetActive(false);
        }
        else
        {
            btn_Left.SetActive(true);
        }

        if (index == AvailableFiles.Count - 1)
        {
            btn_Right.SetActive(false);
        }
        else
        {
            btn_Right.SetActive(true);
        }
    }

    public void ReturnToMenu()
    { 
        SceneManager.LoadScene("Start Menu", LoadSceneMode.Single);
    }
}
