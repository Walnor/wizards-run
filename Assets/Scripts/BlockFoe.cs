﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockFoe : MonoBehaviour, Enemy
{
    int HP = 3;

    public void OnHit(float Power)
    {
        HP--;

        transform.position += Vector3.down;
        if (HP <= 0)
            Destroy(gameObject);
    }

    public new dataFoe.foeType GetType()
    {
        return dataFoe.foeType.Block;
    }
}
