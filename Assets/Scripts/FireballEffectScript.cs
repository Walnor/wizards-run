﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballEffectScript : MonoBehaviour
{
    Vector3 OldPos;
    private void Start()
    {
        OldPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }
    void Update()
    {
        Vector3 dir = transform.position - OldPos;

        transform.LookAt(transform.position + dir);
        OldPos = transform.position;
           
    }
}
