﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageColorLerp : MonoBehaviour
{
    public Image m_imageScript;

    public float m_rate = 1f;

    public Color m_ColorA;
    public Color m_ColorB;

    float m_timer = 0f;

    bool InToggle = true;

    private void Update()
    {
        if (InToggle)
        {
            m_timer += (Time.deltaTime / m_rate);
            if (m_timer >= 1f)
            {
                m_timer = 1f;
                InToggle = !InToggle;
            }
        }
        else
        {
            m_timer -= (Time.deltaTime / m_rate);
            if (m_timer <= 0f)
            {
                m_timer = 0f;
                InToggle = !InToggle;
            }

        }

        Color C = new Color();

        C.r = Mathf.Lerp(m_ColorA.r, m_ColorB.r, m_timer);
        C.g = Mathf.Lerp(m_ColorA.g, m_ColorB.g, m_timer);
        C.b = Mathf.Lerp(m_ColorA.b, m_ColorB.b, m_timer);
        C.a = Mathf.Lerp(m_ColorA.a, m_ColorB.a, m_timer);

        m_imageScript.color = C;
    }
}
