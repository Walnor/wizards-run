﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PewFoe : MonoBehaviour, Enemy
{
    Player target = null;
    public GameObject m_projectile;

    float m_Stun = 0f;

    float m_counter = 0f;

    public float m_FireRate = 0.5f;

    public float m_FireDistance = 15f;

    public float m_FireSpeed = 30f;

    private void Update()
    {

        if (target == null)
        {
            target = findPlayer();
            if (target == null)
                return;
        }
        if (Time.timeScale == 0f || target.EndState)
            return;
        if (m_Stun > 0f)
        {
            StallState();
        }
        else
        {
            ShootState();
        }

    }

    private void StallState()
    {
        m_Stun -= Time.deltaTime;
    }

    private void ShootState()
    {
        Vector3 dir = target.gameObject.transform.position - transform.position;
        dir.y = 0f;

        float dist = dir.magnitude;

        if (dist <= m_FireDistance)
        {
            m_counter += Time.deltaTime;

            if (m_counter >= m_FireRate)
            {
                m_counter = 0f;
                fire(dir.normalized);
            }
        }
    }

    public void fire(Vector3 dir)
    {
        GameObject proOBJ = Instantiate(m_projectile, transform.position + dir, new Quaternion());

        PewFoe_Projectile proCom = proOBJ.GetComponent<PewFoe_Projectile>();
        proCom.Init(dir, m_FireSpeed);
    }

    public void OnHit(float Power)
    {
        m_Stun += Power * 2f;
    }
    
    private Player findPlayer()
    {
        return GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    public new dataFoe.foeType GetType()
    {
        return dataFoe.foeType.Pew;
    }
}
