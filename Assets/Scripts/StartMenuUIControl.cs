﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartMenuUIControl : MonoBehaviour
{

    private void Start()
    {        
        FindObjectOfType<CameraController>().gameObject.transform.position = new Vector3(1000, 0, 0);
    }

    public void PlayBTN()
    {
        SceneManager.LoadScene("GameWorld", LoadSceneMode.Single);
    }
    public void PlayStageSetBTN()
    {
        SceneManager.LoadScene("GameWorldStageSets", LoadSceneMode.Single);
    }

    public void StageCreatorBTN()
    {
        SceneManager.LoadScene("StageCreator", LoadSceneMode.Single);
    }

    public void SetBuilderBTN()
    {
        SceneManager.LoadScene("SetBuilder", LoadSceneMode.Single);
    }

    public void ExitBTN()
    {
        Application.Quit();
    }
}
