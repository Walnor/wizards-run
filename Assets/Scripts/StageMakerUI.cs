﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System;

public class StageMakerUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public StageMaker m_StageMaker;

    public GameObject TagBTNPrefab;
    public GameObject BackBTNPrefab;

    public GameObject Layer1;

    public List<GameObject> m_Enemies;
    public List<GameObject> m_Pickups;
    public List<GameObject> m_Blocks;

    public Text NameText;

    public int state = 0;
    private int oldState = 0;

    private Queue<GameObject> uiList;

    private Vector3 startUiPos = new Vector3(150, 500);
    private Vector3 AdjustAmount = new Vector3(0, -50);

    private void Update()
    {
        if (state != oldState && uiList != null && uiList.Count > 0)
        {
            while (uiList.Count > 0)
            {
                GameObject obj = uiList.Dequeue();
                Destroy(obj);
            }
        }
        if (state == 1)
        {
            Layer1.SetActive(false);
            MakeState1Menu();
            return;
        }
        if (state == 2)
        {
            Layer1.SetActive(false);
            MakeState2Menu();
            return;
        }
        if (state == 3)
        {
            Layer1.SetActive(false);
            MakeState3Menu();
            return;
        }
        if (state == 4)
        {
            Layer1.SetActive(false);
            MakeState4Menu();
            return;
        }
        oldState = 0;
        Layer1.SetActive(true);
    }

    private void MakeState4Menu()
    {
        if (oldState != state)
        {
            uiList = new Queue<GameObject>();
            BTNPrefabGeneric(0, "Telleport").onClick.AddListener(delegate 
            {
                m_StageMaker.m_Mode = StageMaker.MakerMode.PlaceTelleport;
            });

            MakeBackBTN();

            oldState = state;
        }
    }

    private void MakeState3Menu()
    {
        if (oldState != state)
        {
            uiList = new Queue<GameObject>();
            for (int i = 0; i < m_Pickups.Count; i++)
            {
                BTNPrefabMakePickup(i, m_Pickups[i].name, m_Pickups[i]);
            }

            MakeBackBTN();

            oldState = state;
        }
    }

    private void MakeState2Menu()
    {
        if (oldState != state)
        {
            uiList = new Queue<GameObject>();
            for (int i = 0; i < m_Blocks.Count; i++)
            {
                BTNPrefabMakeBlock(i, m_Blocks[i].name, m_Blocks[i]);
            }

            MakeBackBTN();

            oldState = state;
        }
    }

    private void MakeState1Menu()
    {
        if (oldState != state)
        {
            uiList = new Queue<GameObject>();
            for (int i = 0; i < m_Enemies.Count; i++)
            {
                BTNPrefabMakeFoe(i, m_Enemies[i].name, m_Enemies[i]);
            }
            MakeBackBTN();

            oldState = state;
        }
    }

    private void MakeBackBTN()
    {
        GameObject obj = Instantiate(BackBTNPrefab, transform);
        obj.GetComponent<Button>().onClick.AddListener(StateReturn);
        obj.transform.position = new Vector3(200, 50);
        obj.transform.localScale = new Vector3(5, 2);
        uiList.Enqueue(obj);
    }
    public void BTNPickupSelect(GameObject P)
    {
        GameObject obj = Instantiate(P, transform);

        Pickup Pick = obj.GetComponent<Pickup>();

        if (Pick == null)
            return;

        Pickup.type T = Pick.m_Type;

        Destroy(obj);

        if (T == Pickup.type.SpeedBoost)
            MakePickupSpeed();
    }
    public void BTNBlockSelect(GameObject enemyBase)
    {
        GameObject obj = Instantiate(enemyBase, transform);

        FloorTile Block = obj.GetComponent<FloorTile>();

        if (Block == null)
            return;

        FloorTile.FloorType T = Block.m_FloorType;

        Destroy(obj);

        if (T == FloorTile.FloorType.Normal)
            MakeTilesMode();
        else
        if (T == FloorTile.FloorType.Ice)
            MakeIceTilesMode();
        else
        if (T == FloorTile.FloorType.Goal)
            SelectGoalMode();
        else
        if (T == FloorTile.FloorType.FastFall)
            SelectFastFallMode();
    }

    public void SelectFastFallMode()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.MakeFastFallTiles;
    }

    public void SelectDeleteMode()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.Delete;
    }

    public void BTNEnemySelect(GameObject enemyBase)
    {
        GameObject obj = Instantiate(enemyBase, transform);

        Enemy foe = obj.GetComponent<Enemy>();

        if (foe == null)
            return;

        dataFoe.foeType T = foe.GetType();

        Destroy(obj);

        if (T == dataFoe.foeType.Pew)
            PlaceFoe_Pew();
        else
        if (T == dataFoe.foeType.Push)
            PlaceFoe_Push();
        else
        if (T == dataFoe.foeType.Bumb)
            PlaceFoe_Bumb();
        else
        if (T == dataFoe.foeType.Block)
            PlaceFoe_Block();
    }

    public Button BTNPrefabGeneric(int adj, string Text)
    {
        GameObject obj = Instantiate(TagBTNPrefab, transform);
        obj.transform.localScale = new Vector3(5, 2, 1);

        obj.transform.position = startUiPos + (AdjustAmount * adj);

        Button btn = obj.GetComponentInChildren<Button>();
        Text txt = obj.GetComponentInChildren<Text>();

        txt.text = Text;

        uiList.Enqueue(obj);

        return btn;
    }

    public void BTNPrefabMakeBlock(int adj, string Text, GameObject block)
    {
        GameObject obj = Instantiate(TagBTNPrefab, transform);
        obj.transform.localScale = new Vector3(5, 2, 1);

        obj.transform.position = startUiPos + (AdjustAmount * adj);

        Button btn = obj.GetComponentInChildren<Button>();
        Text txt = obj.GetComponentInChildren<Text>();
        btn.onClick.AddListener(delegate { BTNBlockSelect(block); });

        txt.text = Text;

        uiList.Enqueue(obj);
    }

    public void BTNPrefabMakeFoe(int adj, string Text, GameObject foe)
    {
        GameObject obj = Instantiate(TagBTNPrefab, transform);
        obj.transform.localScale = new Vector3(5, 2, 1);

        obj.transform.position = startUiPos + (AdjustAmount * adj);

        Button btn = obj.GetComponentInChildren<Button>();
        Text txt = obj.GetComponentInChildren<Text>();
        btn.onClick.AddListener(delegate { BTNEnemySelect(foe); });

        txt.text = Text;

        uiList.Enqueue(obj);
    }

    public void BTNPrefabMakePickup(int adj, string Text, GameObject Pick)
    {
        GameObject obj = Instantiate(TagBTNPrefab, transform);
        obj.transform.localScale = new Vector3(5, 2, 1);

        obj.transform.position = startUiPos + (AdjustAmount * adj);

        Button btn = obj.GetComponentInChildren<Button>();
        Text txt = obj.GetComponentInChildren<Text>();
        btn.onClick.AddListener(delegate { BTNPickupSelect(Pick); });

        txt.text = Text;

        uiList.Enqueue(obj);
    }

    public void StateSelect1()
    {
        state = 1;
    }
    public void StateSelect2()
    {
        state = 2;
    }
    public void StateSelect3()
    {
        state = 3;
    }
    public void StateSelect4()
    {
        state = 4;
    }

    public void StateReturn()
    {
        state = 0;
    }
    public void MakePickupSpeed()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.PickupSpeed;
    }

    public void MakeTilesMode()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.MakeTiles;
    }
    public void MakeIceTilesMode()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.MakeIceTiles;
    }

    public void SelectGoalMode()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.SelectGoal;
    }

    public void PlaceFoe_Push()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.PlaceFoePush;
    }

    public void PlaceFoe_Pew()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.PlaceFoePew;
    }
    public void PlaceFoe_Block()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.PlaceFoeBlock;
    }

    public void PlaceFoe_Bumb()
    {
        m_StageMaker.m_Mode = StageMaker.MakerMode.PlaceBumbPew;
    }

    public void Save()
    {
        string StringFile = m_StageMaker.TurnToStageFile();

        List<string> rows = new List<string>();
        rows.AddRange(StringFile.Split('\n'));


        string filePath = @".\Assets\StageFiles\" + NameText.text + ".txt";

        FileStream fs = File.Create(filePath);

        StreamWriter sw = new StreamWriter(fs);
        sw.Flush();

        foreach (string s in rows)
        {
            sw.WriteLine(s);
        }

        sw.Close();
        Debug.Log(StringFile);
    }

    public void ChangeName()
    {
        Debug.Log("The name is now: ");
    }

    public void TabToggle()
    {
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        m_StageMaker.MouseIsOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        m_StageMaker.MouseIsOver = false;
    }

    public void ToMainMenu()
    {
        SceneManager.LoadScene("Start Menu", LoadSceneMode.Single);
    }
}
