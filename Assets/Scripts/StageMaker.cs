﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageMaker : MonoBehaviour
{
    public enum MakerMode
    {
        MakeTiles, MakeIceTiles, SelectGoal, PlaceFoePush, PlaceFoePew, PlaceBumbPew, Null,
        PickupSpeed, MakeFastFallTiles, Delete, PlaceFoeBlock, PlaceTelleport
    }

    public MakerMode m_Mode = MakerMode.MakeTiles;

    public GameObject m_DefaultFloor,m_IceFloor, m_GoalFloor, m_StartFloor, m_FastFallFloor;

    public float m_GridSquareSize = 5f;

    Vector2 StartPos;

    FloorTile m_goal = null;

    List<FloorTile> m_FloorTiles = new List<FloorTile>();

    public bool MouseIsOver = false;

    public Material m_GoalMat, m_NormMat;

    public GameObject PushFoe, PewFoe, BumbFoe, BlockFoe;

    public GameObject PickupSpeed;

    public GameObject Telleport;

    List<GameObject> PewEnemies = new List<GameObject>();
    List<GameObject> PushEnemies = new List<GameObject>();
    List<GameObject> BumbEnemies = new List<GameObject>();
    List<GameObject> BlockEnemies = new List<GameObject>();
    List<GameObject> SpeedPickups = new List<GameObject>();
    List<GameObject> Telleporters = new List<GameObject>();

    private void Start()
    {
        StartPos = new Vector2(transform.position.x, transform.position.z);
        GameObject obj = Instantiate(m_StartFloor, transform.position, new Quaternion());
        FloorTile objFloor = obj.GetComponent<FloorTile>();
        objFloor.givePos(StartPos);
        m_FloorTiles.Add(objFloor);

        FindObjectOfType<CameraController>().gameObject.transform.position = new Vector3(1000, 0, 1000);
    }

    void Update()
    {
        if (MouseIsOver)
            return;

        switch (m_Mode)
        {
            case MakerMode.MakeTiles:
                CreateMode(false);
                break;
            case MakerMode.MakeIceTiles:
                CreateMode(true);
                break;
            case MakerMode.SelectGoal:
                SelectGoal();
                break;
            case MakerMode.PlaceFoePush:
                PlacePushFoe();
                break;
            case MakerMode.PlaceFoePew:
                PlacePewFoe();
                break;
            case MakerMode.PlaceBumbPew:
                PlaceBumbFoe();
                break;
            case MakerMode.PickupSpeed:
                PlaceSpeedPickup();
                break;
            case MakerMode.MakeFastFallTiles:
                FastFallPlace();
                break;
            case MakerMode.Delete:
                DeleteAtClick();
                break;
            case MakerMode.PlaceFoeBlock:
                PlaceBlockFoe();
                break;
            case MakerMode.PlaceTelleport:
                PlaceTelleport();
                break;
            case MakerMode.Null:
                break;
        }

    }

    private void PlaceTelleport()
    {
        PlaceObject(Telleport, Telleporters);
    }

    private void PlaceBlockFoe()
    {
        PlaceObject(BlockFoe, BlockEnemies);
    }

    private bool GetClosestPoint(out Vector3 ClosestPoint)
    {
        if (Input.GetMouseButtonDown(0))
        {
            Plane plane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float RayHit;
            if (plane.Raycast(ray, out RayHit))
            {
                Vector3 hitPoint = ray.GetPoint(RayHit);

                Vector3 LB = hitPoint - new Vector3(hitPoint.x % m_GridSquareSize, 0, hitPoint.z % m_GridSquareSize);
                Vector3 LT = hitPoint - new Vector3(hitPoint.x % m_GridSquareSize, 0, -1 * (m_GridSquareSize - (hitPoint.z % m_GridSquareSize)));
                Vector3 RB = hitPoint - new Vector3(-1 * (m_GridSquareSize - (hitPoint.x % m_GridSquareSize)), 0, hitPoint.z % m_GridSquareSize);
                Vector3 RT = hitPoint - new Vector3(-1 * (m_GridSquareSize - (hitPoint.x % m_GridSquareSize)), 0, -1 * (m_GridSquareSize - (hitPoint.z % m_GridSquareSize)));

                ClosestPoint = LB;
                float CurrentDist = (LB - hitPoint).magnitude;

                if ((LT - hitPoint).magnitude < CurrentDist)
                {
                    ClosestPoint = LT;
                    CurrentDist = (LT - hitPoint).magnitude;
                }

                if ((RB - hitPoint).magnitude < CurrentDist)
                {
                    ClosestPoint = RB;
                    CurrentDist = (RB - hitPoint).magnitude;
                }

                if ((RT - hitPoint).magnitude < CurrentDist)
                {
                    ClosestPoint = RT;
                    CurrentDist = (RT - hitPoint).magnitude;
                }

                return true;
            }
            
        }
        ClosestPoint = Vector3.zero;
        return false;
    }

    private void DeleteAtClick()
    {
        Vector3 ClosestPoint;
        if (GetClosestPoint(out ClosestPoint))
        {
            if (RemoveEntityAt(ClosestPoint, PushEnemies) ||
                RemoveEntityAt(ClosestPoint, PewEnemies) ||
                RemoveEntityAt(ClosestPoint, BumbEnemies) ||
                RemoveEntityAt(ClosestPoint, SpeedPickups) ||
                RemoveEntityAt(ClosestPoint, Telleporters))
                return;

            for (int i = 0; i < m_FloorTiles.Count; i++)
            {
                Vector3 v = m_FloorTiles[i].transform.position;
                if (ClosestPoint.x == v.x && ClosestPoint.z == v.z)
                {
                    FloorTile obj = m_FloorTiles[i];
                    m_FloorTiles.Remove(obj);
                    Destroy(obj.gameObject);
                }
            }
        }
    }

    public bool RemoveEntityAt(Vector3 pos, List<GameObject> list)
    {

        for (int i = 0; i < list.Count; i++)
        {
            Vector3 v = list[i].transform.position;
            if (pos.x == v.x && pos.z == v.z)
            {
                GameObject obj = list[i];
                list.Remove(obj);
                Destroy(obj);
                return true;
            }
        }
        return false;
    }

    private void FastFallPlace()
    {
        PlaceFloor(m_FastFallFloor);
    }

    private void PlaceSpeedPickup()
    {
        PlaceObject(PickupSpeed, SpeedPickups);
    }

    private void PlaceFloor(GameObject obj)
    {
        Vector3 ClosestPoint;
        if (GetClosestPoint(out ClosestPoint))
        {
            for (int i = 0; i < m_FloorTiles.Count; i++)
            {
                if (!SpotState(ClosestPoint))
                {
                    GameObject ob = Instantiate(obj, ClosestPoint, new Quaternion());
                    FloorTile objFloor = ob.GetComponent<FloorTile>();
                    objFloor.givePos(ClosestPoint);
                    m_FloorTiles.Add(objFloor);
                }
            }
        }
    }

    void CreateMode(bool MakeIce)
    {
        if (MakeIce)
            PlaceFloor(m_IceFloor);
        else
            PlaceFloor(m_DefaultFloor);
    }

    void SelectGoal()
    {
        Vector3 ClosestPoint;
        if (GetClosestPoint(out ClosestPoint))
        {
            if (SpotState(ClosestPoint) && SpotFloorType(ClosestPoint) == FloorTile.FloorType.Normal)
            {
                if (m_goal != null)
                {
                    m_goal.m_FloorType = FloorTile.FloorType.Normal;
                    m_goal.GetComponentInChildren<MeshRenderer>().material = m_NormMat;
                }

                foreach (FloorTile t in m_FloorTiles)
                {
                    if (t.PosCheck(ClosestPoint))
                    {
                        m_goal = t.GetComponent<FloorTile>();
                        t.m_FloorType = FloorTile.FloorType.Goal;
                        t.GetComponentInChildren<MeshRenderer>().material = m_GoalMat;
                        break;
                    }
                }
            }
        }
    }

    void PlacePushFoe()
    {
        PlaceObject(PushFoe, PushEnemies);
    }

    void PlacePewFoe()
    {
        PlaceObject(PewFoe, PewEnemies);
    }


    private void PlaceBumbFoe()
    {
        PlaceObject(BumbFoe, BumbEnemies);
    }

    private void PlaceObject(GameObject obj, List<GameObject> List)
    {
        Vector3 ClosestPoint;
        if (GetClosestPoint(out ClosestPoint))
        {
            ClosestPoint.y += 2f;
            GameObject OBJ = Instantiate(obj, ClosestPoint, new Quaternion());

            List.Add(OBJ);
        }
    }

    void TestDraw(Vector3 point)
    {
        DebugCross(point, Color.red);

        point.x -= point.x % m_GridSquareSize;

        DebugCross(point, Color.green);

        point.z -= point.z % m_GridSquareSize;

        DebugCross(point, Color.cyan);
    }

    void DebugCross(Vector3 point, Color c)
    {
        Debug.DrawRay(point - (Vector3.right / 2f), Vector3.right, c);
        Debug.DrawRay(point - (Vector3.back / 2f), Vector3.back, c);
    }

    public string TurnToStageFile()
    {
        float LowestX = StartPos.x;
        float HighestX = StartPos.x;

        float LowestY = StartPos.y;
        float HighestY = StartPos.y;

        foreach (FloorTile t in m_FloorTiles)
        {
            if (t.Xpos < LowestX) LowestX = t.Xpos;
            if (t.Xpos > HighestX) HighestX = t.Xpos;
            if (t.Ypos < LowestY) LowestY = t.Ypos;
            if (t.Ypos > HighestY) HighestY = t.Ypos;
        }

        Debug.Log("Lowest X: " + LowestX + " Highest X: " + HighestX);
        Debug.Log("Lowest Y: " + LowestY + " Highest Y: " + HighestY);
        string NewFile = "";
        for (int x = (int)HighestX; x >= LowestX; x -= 5)
        {
            for (int y = (int)LowestY; y <= HighestY; y += 5)
            {
                FloorTile.FloorType type = SpotFloorType(new Vector3(x, 0, y));
                if (type == FloorTile.FloorType.Normal)
                    NewFile += "1";
                else
                if (type == FloorTile.FloorType.Ice)
                    NewFile += "I";
                else
                if (type == FloorTile.FloorType.Goal)
                    NewFile += "G";
                else
                if (type == FloorTile.FloorType.Start)
                    NewFile += "S";
                else
                if (type == FloorTile.FloorType.FastFall)
                    NewFile += "F";
                else
                    NewFile += "0";
            }
            NewFile += "\n";
        }
        NewFile += "=\n";

        foreach (GameObject o in PushEnemies)
        {
            NewFile += "P ";
            NewFile += PosString((int)LowestX, (int)LowestY, o);
        }

        foreach (GameObject o in PewEnemies)
        {
            NewFile += "E ";
            NewFile += PosString((int)LowestX, (int)LowestY, o);
        }

        foreach (GameObject o in BumbEnemies)
        {
            NewFile += "B ";
            NewFile += PosString((int)LowestX, (int)LowestY, o);
        }

        foreach (GameObject o in SpeedPickups)
        {
            NewFile += "s ";
            NewFile += PosString((int)LowestX, (int)LowestY, o);
        }

        foreach (GameObject o in BlockEnemies)
        {
            NewFile += "K ";
            NewFile += PosString((int)LowestX, (int)LowestY, o);
        }

        for (int i = 1; i < Telleporters.Count; i += 2)
        {
            NewFile += "T ";
            NewFile += PosString((int)LowestX, (int)LowestY, Telleporters[i], false);
            NewFile += " ";
            NewFile += PosString((int)LowestX, (int)LowestY, Telleporters[i - 1]);

        }

        return NewFile;
    }

    private string PosString(int LowestX, int LowestY,GameObject o, bool Enter = true)
    {
        int X = (int)o.transform.position.x;
        int Y = (int)o.transform.position.z;

        X -= LowestX;
        Y -= LowestY;

        X = X / 5;
        Y = Y / 5;

        string toReturn = "" + X + " " + Y;
        if (Enter)
            toReturn += "\n";

        return toReturn;
    }

    private bool SpotState(Vector3 pos)
    {
        foreach (FloorTile t in m_FloorTiles)
        {
            if (t.PosCheck(pos))
                return true;
        }
        return false;
    }

    private FloorTile.FloorType SpotFloorType(Vector3 pos)
    {
        foreach (FloorTile t in m_FloorTiles)
        {
            if (t.PosCheck(pos))
                return t.m_FloorType;
        }
        return FloorTile.FloorType.Null;
    }
}
