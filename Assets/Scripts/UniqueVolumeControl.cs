﻿using System.Collections.Generic;
using UnityEngine;
class UniqueVolumeControl : MonoBehaviour
{
    List<AudioSource> audios = new List<AudioSource>();

    public float MaxVolume = 1.0f;

    private void LateUpdate()
    {
        if (audios.Count < 3)
        {
            audios.AddRange(GetComponents<AudioSource>());
        }

        foreach (AudioSource AS in audios)
        {
            if (AS.volume >= MaxVolume)
                AS.volume = MaxVolume;
        }
    }
}