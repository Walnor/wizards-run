﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieInX : MonoBehaviour
{
    public float m_X = 1f;

    float timer = 0f;

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= m_X)
            Destroy(gameObject);
        
    }
}
