﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject m_player = null;

    public static GameObject Cam = null;

    private void Start()
    {
        DontDestroyOnLoad(gameObject);

        if (Cam != null)
        {
            Destroy(gameObject);
            return;
        }

        Cam = gameObject;
    }

    private void Update()
    {
        if (m_player == null)
        {
            LoneCam();
            return;
        }
        Vector3 currentPos = transform.position;
        Vector3 TargetPos = m_player.transform.position;
        TargetPos.y = currentPos.y;

        float TarX = TargetPos.x - currentPos.x;
        float TarZ = TargetPos.z - currentPos.z;

        if (Mathf.Abs(TarX) >= 3f)
        {
            currentPos.x += (Time.deltaTime * TarX * 1.5f);
        }

        if (Mathf.Abs(TarZ) >= 3f)
        {
            currentPos.z += (Time.deltaTime * TarZ * 1.5f);
        }

        transform.position = currentPos;

    }

    private void LoneCam()
    {

        float X_Speed = 0f;
        float Z_Speed = 0f;

        if (Input.GetKey(KeyCode.A))
        {
            Z_Speed += 1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            Z_Speed -= 1;
        }

        if (Input.GetKey(KeyCode.W))
        {
            X_Speed += 1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            X_Speed -= 1;
        }

        Vector3 newPos = gameObject.transform.position;

        newPos.x += (X_Speed * Time.deltaTime * 10f);
        newPos.z += (Z_Speed * Time.deltaTime * 10f);

        gameObject.transform.position = newPos;
    }
}
