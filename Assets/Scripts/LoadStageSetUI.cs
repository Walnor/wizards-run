﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadStageSetUI : MonoBehaviour
{
    List<string> FileNames = new List<string>();

    public GameObject btn_Left, btn_Right;
    public GameControlSets m_GC;
    public GameLoadIntermediate m_GLI = null;

    public Text FileToLoad;
    static int Index = 0;

    public bool EndGameState = false;
    private void Start()
    {
        if (!EndGameState)
        {
            m_GLI = FindObjectOfType<GameLoadIntermediate>(); 

            if (m_GLI != null)
            {
                LoadStage();
            }
        }

        DirectoryInfo d = new DirectoryInfo(@".\Assets\StageSetFiles");
        FileInfo[] Files = d.GetFiles("*.txt");
        foreach (FileInfo file in Files)
        {
            FileNames.Add(file.Name);
        }
        EndEffect();
    }

    public void LoadStage()
    {
        if (EndGameState)
        {
            m_GLI.m_Text = @".\Assets\StageSetFiles\" + FileToLoad.text;
            SceneManager.LoadScene("GameWorld", LoadSceneMode.Single);
            Destroy(gameObject);
            return;
        }

        if (m_GLI != null)
        {
            m_GC.init(m_GLI.m_Text);
            Destroy(m_GLI.gameObject);
            Destroy(gameObject);
        }

        m_GC.init(@".\Assets\StageSetFiles\" + FileToLoad.text);
        Destroy(gameObject);
    }

    public void Right()
    {
        Index++;
        EndEffect();
    }

    public void Left()
    {
        Index--;
        EndEffect();
    }

    private void EndEffect()
    {
        FileToLoad.text = FileNames[Index];

        if (Index == 0)
        {
            btn_Left.SetActive(false);
        }
        else
        {
            btn_Left.SetActive(true);
        }

        if (Index == FileNames.Count - 1)
        {
            btn_Right.SetActive(false);
        }
        else
        {
            btn_Right.SetActive(true);
        }
    }

    public void ReturnToMenu()
    {
        SceneManager.LoadScene("Start Menu", LoadSceneMode.Single);
    }
}