﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PewFoe_Projectile : MonoBehaviour
{
    Vector3 m_Direction;
    float m_Speed;

    float m_life = 3f;

    public void Init(Vector3 dir, float sp)
    {
        m_Direction = dir;
        m_Speed = sp;
    }

    private void Update()
    {
        transform.position += m_Direction * Time.deltaTime * m_Speed;

        m_life -= Time.deltaTime;

        if (m_life <= 0)
            Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        Player other = collision.gameObject.GetComponent<Player>();

        if (other != null)
        {
            other.m_SpeedMult *= 0.5f;
            Destroy(gameObject);
        }

    }
}