﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class WorldBuilder : MonoBehaviour
{
    public GameObject m_Block;
    public GameObject m_BlockS;
    public GameObject m_BlockG;
    public GameObject m_BlockI;
    public GameObject m_BlockF;
    public GameObject m_PushFoe;
    public GameObject m_BumbFoe;
    public GameObject m_PewFoe;
    public GameObject m_BlockFoe;
    public GameObject m_Telleport;
    public GameObject m_PickupSpeed;
    public float size = 5f;

    int countX = 50;
    int countY = 50;

    public List<List<FloorTile>> tiles;
    public FloorTile StartPoint;

    float XStageOffset = 1000f;

    public void Init(string fileName)
    {
        Debug.Log(fileName);
        string FileToLoad = "";

        DirectoryInfo d = new DirectoryInfo(@".\Assets\StageFiles");
        FileInfo[] Files = d.GetFiles("*.txt");
        foreach (FileInfo f in Files)
        {
            Debug.Log(f.Name.Length + " | | " + fileName.Length);

            if (f.Name.Length <= fileName.Length)
            {
                bool isGood = true;
                for (int i = 0; i < f.Name.Length; i++)
                {
                    if ((int)f.Name[i] == (int)fileName[i])
                        isGood = true;
                    else
                    {
                        isGood = false;
                        break;
                    }
                }
                if (isGood)
                {
                    FileToLoad = @".\Assets\StageFiles\" + f.Name;
                    break;
                }
            }
            
            if (f.Name == fileName)
            {
                FileToLoad = @".\Assets\StageFiles\" + f.Name;
                break;
            }

        }

        string fileText = System.IO.File.ReadAllText(FileToLoad);


        tiles = new List<List<FloorTile>>();
        List<string> FileBreak = new List<string>();
        FileBreak.AddRange(fileText.Split('='));
        List<string> rows = new List<string>();
        rows.AddRange(FileBreak[0].Split('\n'));
        MakeFloors(rows);
        List<string> foes = new List<string>();
        foes.AddRange(FileBreak[1].Split('\n'));


        MakeEntities(foes);
    }

    private void MakeEntities(List<string> foes)
    {
        foreach (string s in foes)
        {
            List<string> StringSplit = new List<string>();
            StringSplit.AddRange(s.Split(' '));

            if (StringSplit.Count == 3 || StringSplit.Count == 5)
            {
                Debug.Log("IsValid");

                if (StringSplit[0] == "E")
                {
                    int X = int.Parse(StringSplit[1]);
                    int Z = int.Parse(StringSplit[2]);

                    GameObject obj = Instantiate(m_PewFoe, new Vector3(((X + 1) * size) + XStageOffset, 2f, Z * size), new Quaternion(),transform);
                }
                else
                if (StringSplit[0] == "P")
                {
                    int X = int.Parse(StringSplit[1]);
                    int Z = int.Parse(StringSplit[2]);

                    GameObject obj = Instantiate(m_PushFoe, new Vector3(((X + 1) * size) + XStageOffset, 2f, Z * size), new Quaternion(), transform);
                }
                else
                if (StringSplit[0] == "B")
                {
                    int X = int.Parse(StringSplit[1]);
                    int Z = int.Parse(StringSplit[2]);

                    GameObject obj = Instantiate(m_BumbFoe, new Vector3(((X + 1) * size) + XStageOffset, 2f, Z * size), new Quaternion(), transform);
                }
                else
                if (StringSplit[0] == "s")
                {
                    int X = int.Parse(StringSplit[1]);
                    int Z = int.Parse(StringSplit[2]);

                    GameObject obj = Instantiate(m_PickupSpeed, new Vector3(((X + 1) * size) + XStageOffset, 2f, Z * size), new Quaternion(), transform);
                }
                else
                if (StringSplit[0] == "K")
                {
                    int X = int.Parse(StringSplit[1]);
                    int Z = int.Parse(StringSplit[2]);

                    GameObject obj = Instantiate(m_BlockFoe, new Vector3(((X + 1) * size) + XStageOffset, 2f, Z * size), new Quaternion(), transform);
                }
                else
                if (StringSplit[0] == "T" && StringSplit.Count == 5)
                {
                    int X1 = int.Parse(StringSplit[1]);
                    int Z1 = int.Parse(StringSplit[2]);
                    int X2 = int.Parse(StringSplit[3]);
                    int Z2 = int.Parse(StringSplit[4]);

                    GameObject obj1 = Instantiate(m_Telleport, new Vector3(((X1 + 1) * size) + XStageOffset, 1f, Z1 * size), new Quaternion(), transform);
                    GameObject obj2 = Instantiate(m_Telleport, new Vector3(((X2 + 1) * size) + XStageOffset, 1f, Z2 * size), new Quaternion(), transform);
                    obj1.GetComponent<Telleport>().m_Other = obj2.GetComponent<Telleport>();
                    obj2.GetComponent<Telleport>().m_Other = obj1.GetComponent<Telleport>();
                }
            }
            else
                Debug.Log("not valid");
        }
    }

    private void MakeFloors(List<string> rows)
    {
        for (int i = 0; i < rows.Count; i++)
        {
            tiles.Add(new List<FloorTile>());
            for (int j = 0; j < rows[i].Length; j++)
            {
                if (rows[i][j] == '1')
                {
                    MakeTile(m_Block, i, j, rows);
                }
                else if (rows[i][j] == 'S')
                {
                    GameObject obj = Instantiate(m_BlockS, transform);
                    obj.transform.position = new Vector3(((rows.Count - i - 1) * size) + XStageOffset, 0, j * size);
                    tiles[i].Add(obj.GetComponent<FloorTile>());
                    obj.GetComponent<FloorTile>().Xpos = i;
                    obj.GetComponent<FloorTile>().Ypos = j;

                    StartPoint = obj.GetComponent<FloorTile>();
                }
                else if (rows[i][j] == 'G')
                {
                    MakeTile(m_BlockG, i, j, rows);
                }
                else if (rows[i][j] == 'I')
                {
                    MakeTile(m_BlockI, i, j, rows);
                }
                else if (rows[i][j] == 'F')
                {
                    MakeTile(m_BlockF, i, j, rows);
                }
            }
        }

        SetTilesHealth();


        for (int i = 0; i < rows.Count; i++)
        {
            tiles.Add(new List<FloorTile>());
            for (int j = 0; j < rows[i].Length; j++)
            {
                if (rows[i][j] == '0')
                {
                    GameObject obj = Instantiate(m_Block, transform);
                    obj.transform.position = new Vector3(((rows.Count - i - 1) * size) + XStageOffset, 0, j * size);

                    obj.GetComponent<FloorTile>().Health = 0.1f;
                }
            }
        }
    }

    private void MakeTile(GameObject t, int i, int j, List<string> rows)
    {
        GameObject obj = Instantiate(t, transform);
        obj.transform.position = new Vector3(((rows.Count - i - 1) * size) + XStageOffset, 0, j * size);
        tiles[i].Add(obj.GetComponent<FloorTile>());
        obj.GetComponent<FloorTile>().Xpos = i;
        obj.GetComponent<FloorTile>().Ypos = j;
    }

    private void SetTilesHealth()
    {
        float Life = 1f;

        FloorTile current = StartPoint;
        StartPoint.Health = Life;
        Life = StartPoint.Health + 1;

        List<FloorTile> toDo = new List<FloorTile>();

        foreach (FloorTile td in GetTileBrothers(current))
        {
            if (td.Health > Life)
            {
                td.Health = Life;
                toDo.AddRange(GetTileBrothers(td));
            }
        }

        Life += 1;

        if (toDo.Count == 0)
            return;

        foreach (FloorTile ft in toDo)
        {
            if (ft.Health > Life)
            {
                ft.Health = Life;
                SetTilesHealth(ft, Life + 1);
            }
        }
    }

    private void SetTilesHealth(FloorTile center, float Life)
    {
        FloorTile current = center;

        List<FloorTile> toDo = new List<FloorTile>();

        foreach (FloorTile td in GetTileBrothers(current))
        {
            if (td.Health > Life)
            {
                td.Health = Life;
                toDo.AddRange(GetTileBrothers(td));
            }
        }

        Life += 1;

        if (toDo.Count == 0)
            return;

        foreach (FloorTile ft in toDo)
        {
            if (ft.Health > Life)
            {
                ft.Health = Life;
                SetTilesHealth(ft, Life + 1);
            }
        }
    }

    private List<FloorTile> GetTileBrothers(FloorTile Center)
    {
        List<FloorTile> toReturn = new List<FloorTile>();

        foreach (FloorTile ft in tiles[Center.Xpos])
        {
            if (ft.Ypos == (Center.Ypos - 1) || ft.Ypos == (Center.Ypos + 1))
            {
                toReturn.Add(ft);
            }
        }

        if (Center.Xpos - 1 >= 0)
        {
            foreach (FloorTile ft in tiles[Center.Xpos - 1])
            {
                if (ft.Ypos == (Center.Ypos))
                {
                    toReturn.Add(ft);
                }
            }
        }

        if (Center.Xpos + 1 < tiles.Count)
        {
            foreach (FloorTile ft in tiles[Center.Xpos + 1])
            {
                if (ft.Ypos == (Center.Ypos))
                {
                    toReturn.Add(ft);
                }
            }
        }

        return toReturn;
    }
}
