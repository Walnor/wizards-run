﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject Projectile;

    public GameObject WarpParticle;

    public Wizard m_Wiz;

    public Material TouchColor;

    public float m_ProjectileSpeed = 20f;

    public float m_Speed = 10f;

    public float m_AddedSpeed = 0f;

    public float m_SpeedMult = 1f;

    bool m_OnIce = false;

    float X_Speed = 0f;
    float Z_Speed = 0f;

    float IceAccel = 0.05f;

    Vector3 ForwardDirection = Vector3.forward;

    float AttackAniTime = 1f;

    public bool EndState = false;

    void Update()
    {
        if (Time.timeScale == 0f || EndState)
            return;

        MovementInput();

        ForwardDirection = new Vector3(X_Speed, 0, Z_Speed);

        if (ForwardDirection.magnitude >= 0.2 && AttackAniTime >= 0.5f)
        {
            m_Wiz.RunAni();
        }
        else
        {
            m_Wiz.IdleAni();
        }

        if (AttackAniTime < 0.5f)
        {
            m_Wiz.AttackAni();
            AttackAniTime += Time.deltaTime;
        }

        ForwardDirection.Normalize();

        Vector3 T = m_Wiz.transform.position;

        T += ForwardDirection;
        m_Wiz.transform.LookAt(T);

    }

    private void MovementInput()
    {

        if (m_AddedSpeed > 0)
            m_AddedSpeed -= (Time.deltaTime * 0.3333f);

        if (m_AddedSpeed < 0)
            m_AddedSpeed = 0;

        if (Input.GetMouseButtonDown(0))
        {
            Plane plane = new Plane(Vector3.up, transform.position);
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            float distance;
            if (plane.Raycast(ray, out distance))
            {
                Vector3 hitPoint = ray.GetPoint(distance);

                Vector3 dir = hitPoint - transform.position;
                dir.y = 0f;

                GameObject proOBJ = Instantiate(Projectile, transform.position + (dir.normalized * 2f), new Quaternion());

                Player_Projectile pp = proOBJ.GetComponent<Player_Projectile>();
                pp.Init(dir.normalized, m_ProjectileSpeed);

                AttackAniTime = 0f;
            }

            m_SpeedMult -= 0.1f;
            if (m_SpeedMult <= 0)
                m_SpeedMult = 0f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            if (Z_Speed < 1f)
            {
                if (!m_OnIce)
                    Z_Speed += 1;
                else
                    Z_Speed += IceAccel;

                if (Z_Speed >= 1f) Z_Speed = 1f;
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (Z_Speed > -1f)
            {
                if (!m_OnIce)
                    Z_Speed -= 1;
                else
                    Z_Speed -= IceAccel;

                if (Z_Speed <= -1f) Z_Speed = -1f;
            }
        }

        if (Input.GetKey(KeyCode.W))
        {
            if (X_Speed < 1f)
            {
                if (!m_OnIce)
                    X_Speed += 1;
                else
                    X_Speed += IceAccel;

                if (X_Speed >= 1f) X_Speed = 1f;
            }
        }
        if (Input.GetKey(KeyCode.S))
        {
            if (X_Speed > -1f)
            {
                if (!m_OnIce)
                    X_Speed -= 1;
                else
                    X_Speed -= IceAccel;

                if (X_Speed <= -1f) X_Speed = -1f;
            }
        }

        Vector3 newPos = gameObject.transform.position;

        newPos.x += (X_Speed * Time.deltaTime * (m_Speed + m_AddedSpeed) * m_SpeedMult);
        newPos.z += (Z_Speed * Time.deltaTime * (m_Speed + m_AddedSpeed) * m_SpeedMult);

        gameObject.transform.position = newPos;

        if (m_SpeedMult <= 1f)
        {
            m_SpeedMult += Time.deltaTime / 5f;

            if (m_SpeedMult >= 1f)
                m_SpeedMult = 1f;
        }


        if (!m_OnIce)
        {
            Z_Speed *= 0.8f;
            X_Speed *= 0.8f;
        }
        else
        {
            Z_Speed *= 0.99f;
            X_Speed *= 0.99f;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        TriggerEffect T = other.GetComponent<TriggerEffect>();
        T.TrigEffect(this);

        Destroy(other.gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        FloorTile floorTile = collision.gameObject.GetComponent<FloorTile>();

        if (floorTile)
        {
            if (floorTile.m_FloorType == FloorTile.FloorType.Normal)
            {
                //floorTile.gameObject.GetComponent<MeshRenderer>().material = TouchColor;
                floorTile.Health /= 2f;
            }

            if (floorTile.m_FloorType == FloorTile.FloorType.Goal)
            {
                floorTile.StageVictory(1f);
            }

            if (floorTile.m_isIce) m_OnIce = true; else m_OnIce = false;
        }
        else
        {
            BumberFoe Bumbfoe = collision.gameObject.GetComponent<BumberFoe>();

            if (Bumbfoe)
            {
                Vector3 dir = Bumbfoe.transform.position - transform.position;
                if (!m_OnIce)
                {
                    X_Speed -= dir.x * Bumbfoe.m_Power;
                    Z_Speed -= dir.z * Bumbfoe.m_Power;
                }
                else
                {
                    X_Speed -= dir.x * (Bumbfoe.m_Power / 3f);
                    Z_Speed -= dir.z * (Bumbfoe.m_Power / 3f);
                }
            }
        }
    }

    public void Win()
    {
        GameObject ptcl = Instantiate(WarpParticle);
        ptcl.transform.position = transform.position + new Vector3(0, -0.5f, 0);

        Destroy(gameObject);
    }
}
