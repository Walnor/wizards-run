﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControlSets : MonoBehaviour
{
    WorldBuilder m_World_Builder = null;

    public GameObject m_Player;

    CameraController m_CamBoon = null;

    GameObject m_PlayerInstance = null;

    int Index = 0;

    List<string> World = new List<string>();

    bool StageLoaded = true;

    bool GameInPlay = false;
    public void init(string file)
    {
        m_CamBoon = FindObjectOfType<CameraController>();

        string fileText = System.IO.File.ReadAllText(file);

        World.AddRange(fileText.Split('\n'));

        DontDestroyOnLoad(gameObject);

        SceneManager.LoadScene("GameWorldSets2", LoadSceneMode.Single);

        StageLoaded = false;
    }

    private void LoadNextStage()
    {
        if (Index < World.Count)
        {
            string fileToLoad = "" + World[Index];

            if (fileToLoad == "")
            {
                SetWin();
                return;
            }

            StageCleanup();
            LoadStage(fileToLoad);
        }
        else
        {
            SetWin();
        }
    }

    private void SetWin()
    {
        Debug.Log("World Complete!");
        SceneManager.LoadScene("SetWinMenu", LoadSceneMode.Single);
        Destroy(gameObject);
    }

    private void StageCleanup()
    {
        if (m_World_Builder == null)
            return;
        /*
        if (m_PlayerInstance != null)
        {
            Destroy(m_PlayerInstance);
            m_PlayerInstance = null;
        }

        if (m_World_Builder != null)
        {
            Destroy(m_World_Builder);
            m_World_Builder = null;
        }*/
        foreach (Transform child in m_World_Builder.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    private void LateUpdate()
    {
        if (!StageLoaded)
        {
            LoadNextStage();
        }
    }

    private void LoadStage(string file)
    {
        m_World_Builder = GameObject.FindGameObjectWithTag("WorldMaker").GetComponent<WorldBuilder>();

        m_World_Builder.Init(file);

        m_PlayerInstance = Instantiate(m_Player);
        m_PlayerInstance.transform.position = m_World_Builder.StartPoint.transform.position + (Vector3.up * 3);

        Vector3 boonStart = m_PlayerInstance.transform.position;
        boonStart.y = 0;

        m_CamBoon.m_player = m_PlayerInstance;
        m_CamBoon.transform.position = boonStart;
        GameInPlay = true;

        Index++;

        StageLoaded = true;
    }

    private void Update()
    {
        if (!GameInPlay)
            return;

        if (m_PlayerInstance == null)
            return;

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (Time.timeScale == 1f)
                Time.timeScale = 0f;
            else
                Time.timeScale = 1f;
        }

        if (m_PlayerInstance.transform.position.y <= -2f)
        {//Come Back Here Soon!!!
            //m_PlayerInstance.GetComponent<Player>().EndState = true;
            //SceneManager.LoadScene("DeadMenu", LoadSceneMode.Additive);
            Index--;
            Destroy(m_PlayerInstance);
            m_PlayerInstance = null;
            Debug.Log("The Player Lost");
            StageLoaded = false;
            //Destroy(gameObject);
            return;
        }
        else if (m_PlayerInstance.transform.position.y >= 5f)
        {
            //m_PlayerInstance.GetComponent<Player>().EndState = true;
            m_PlayerInstance.GetComponent<Player>().Win();
            Destroy(m_PlayerInstance);
            m_PlayerInstance = null;
            Debug.Log("The Player Won");
            StageLoaded = false;
            //SceneManager.LoadScene("WinMenu", LoadSceneMode.Additive);
            //Destroy(gameObject);
            return;
        }

        if ((m_PlayerInstance.transform.position.y <= -2f) || (m_PlayerInstance.transform.position.y >= 5f))
        {
            //SceneManager.LoadScene("GameWorld", LoadSceneMode.Single);
        }
    }
}