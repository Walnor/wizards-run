﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumberFoe : MonoBehaviour, Enemy
{
    [HideInInspector] public float m_Power = 7f;
    private void Update()
    {
        if (Time.timeScale == 0f)
            return;

        transform.Rotate(Vector3.up, 90.0f * Time.deltaTime);
    }
    
    public new dataFoe.foeType GetType()
    {
        return dataFoe.foeType.Bumb;
    }

    public void OnHit(float Power)
    {
        return;
    }
}
