﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour, TriggerEffect
{
    public enum type
    {
        SpeedBoost, nil
    }

    public type m_Type = type.SpeedBoost;

    bool FloatUp = true;

    float timer = 0;
    float MoveAmount = 1.5f;
    private void Start()
    {
    }

    private void Update()
    {
        if (FloatUp)
        {
            transform.position += transform.up * Time.deltaTime * MoveAmount;
            timer += Time.deltaTime;
            if (timer >= 1f)
                FloatUp = false;
        }
        else
        {
            transform.position -= transform.up * Time.deltaTime * MoveAmount;
            timer -= Time.deltaTime;
            if (timer < 0)
                FloatUp = true;
        }
    }

    public void TrigEffect(Player p)
    {
        if (m_Type == type.SpeedBoost)
            p.m_AddedSpeed += 5f;
    }
}
