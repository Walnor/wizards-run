﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class dataFoe
{
    public enum foeType
    {
        Push, Pew, Bumb, Block
    }
}

interface Enemy
{
    void OnHit(float Power);
    dataFoe.foeType GetType();
    
}