﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game_Controller : MonoBehaviour
{

    public WorldBuilder m_World_Builder;

    public GameObject m_Player;

    CameraController m_CamBoon = null;

    GameObject m_PlayerInstance;

    bool GameInPlay = false;
    public void init(string file) 
    {
        m_CamBoon = FindObjectOfType<CameraController>();

        m_World_Builder.Init(file);

        m_PlayerInstance = Instantiate(m_Player);
        m_PlayerInstance.transform.position = m_World_Builder.StartPoint.transform.position + (Vector3.up * 3);

        Vector3 boonStart = m_PlayerInstance.transform.position;
        boonStart.y = 0;

        m_CamBoon.m_player = m_PlayerInstance;
        m_CamBoon.transform.position = boonStart;
        GameInPlay = true;
    }

    private void Update()
    {
        if (!GameInPlay)
            return;

        if (Input.GetKeyDown(KeyCode.P))
        {
            if (Time.timeScale == 1f)
                Time.timeScale = 0f;
            else
                Time.timeScale = 1f;
        }

        if (m_PlayerInstance.transform.position.y <= -2f)
        {
            m_PlayerInstance.GetComponent<Player>().EndState = true;
            SceneManager.LoadScene("DeadMenu", LoadSceneMode.Additive);
            Debug.Log("The Player Lost");
            Destroy(gameObject);
            return;
        }
        else if (m_PlayerInstance.transform.position.y >= 5f)
        {
            m_PlayerInstance.GetComponent<Player>().EndState = true;
            m_PlayerInstance.GetComponent<Player>().Win();
            Debug.Log("The Player Won");
            SceneManager.LoadScene("WinMenu", LoadSceneMode.Additive);
            Destroy(gameObject);
            return;
        }

        if ((m_PlayerInstance.transform.position.y <= -2f) || (m_PlayerInstance.transform.position.y >= 5f))
        {
            SceneManager.LoadScene("GameWorld", LoadSceneMode.Single);
        }
    }
}
